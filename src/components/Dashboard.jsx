import React from 'react';
import BlogItemList from './BlogItemList.jsx'
import { Grid, Col, Label, Button } from 'react-bootstrap'

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: 'Inital input value'};

  }

  render() {
    return (
      <Grid>
		<Col lg={8} md={4}>
			<h1><Label bsStyle='success'>#dajsiepoznacHeartbeat</Label></h1>	
			<BlogItemList />
		</Col>
	  </Grid>
    );
  }
};

export default Dashboard;