import React from 'react';
import { ListGroupItem, Button, Grid, Col } from 'react-bootstrap';
import format from 'date-format';


class BlogItem extends React.Component {
  constructor(props) {
    super(props);
  }
  
  _getTitle (entries) {
	  if (entries.length > 0) {
		  return entries[0].title;
	  }
  }
  
  _getLastPostDate () {
	  if (this.props.details.entries.length > 0) {
		if (this.props.details.entries[0].publishedDate != '') {
			return format.asString('yyyy-MM-dd hh:mm:ss', new Date(this.props.details.entries[0].publishedDate));
		  }
		  return 'Unknown';
	  } else {
		  return 'Never';
	  }
  }
  
  _getItemStyle (whichWeek) {
	  switch (whichWeek) {
		  case 'thisWeek':
			return 'success';
		  case 'lastWeek':
		    return 'warning';
		  case 'longTimeAgo':
		    return 'danger';
		  default:
		    return 'info';
	  }
  }
  
  render() {

    return (
      <ListGroupItem bsStyle={ this._getItemStyle(this.props.details.whichWeek) }>
		<Grid>
			<Col xs={8} md={5}>
				<p><b>Blog Title: {this.props.details.title}</b></p>
				<div>Posts: {this.props.details.entries.length} <b>#dajsiepoznac: {this.props.details.dspPostsCount }</b></div>
				<div>Last Post Date: { this._getLastPostDate() }</div>
				<div>Last Post Title: {this._getTitle(this.props.details.entries)}</div>
			</Col>	
			<Col xs={1} md={1}>
				<Button bsSize='large' key={ this.props.keyForButton + 20 } bsStyle='warning' href={this.props.details.rssAddress}><i className='icon ion-social-rss'/></Button>
			</Col>
			<Col xs={1} md={1}>
				<Button bsSize='large' key={ this.props.keyForButton + 10 } bsStyle='success' href={this.props.details.blogAddress}><i className='ion-arrow-right-a'/></Button>
			</Col>
			
		</Grid>
	  </ListGroupItem>
    );
  }
};

export default BlogItem;