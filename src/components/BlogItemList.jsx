import React from 'react';
import BlogItem from './BlogItem.jsx';
import $ from 'jquery';
import { ListGroup, ProgressBar, Label } from 'react-bootstrap';
import format from 'date-format';
import { LocalDateTime, LocalDate, Period } from 'js-joda'

class BlogUpdate {
	constructor(title, lastPostAddress, blogAddress, rssAddress, entries, lastPostDate, dspPostsCount, whichWeek) {
		this.title = title;
		this.lastPostAddress = lastPostAddress;
		this.blogAddress = blogAddress;
		this.rssAddress = rssAddress;
		this.entries = entries;
		this.dspPostsCount = dspPostsCount;
		this.lastPostDate = lastPostDate;
		this.whichWeek = whichWeek;
	}
}

class BlogItemList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
		lessThan7Count : 0,
		lessThan14Count : 0,
		moreThan13Count : 0,
		notActiveCount : 0,
		blogDetails : []
		};
	this.attendeesFeeds = [
			'http://0x00antdot.blogspot.com/feeds/posts/default',
			'http://adambac.com/feed.xml',
			'http://adamszneider.azurewebsites.net/feed/',
			'http://addictedtocreating.pl/feed/',
			'http://agatamroz.com.pl/feed/',
			'http://aragornziel.blogspot.com/feeds/posts/default',
			'http://arekbal.blogspot.com/feeds/posts/default',
			'http://art-of-ai.com/feed/',
			'http://bartmalanczuk.github.io/feed.xml',
			'http://bartoszrowinski.pl/feed/',
			'http://bga.pl/index.php/feed/',
			'http://blog.buczaj.com/feed/',
			'http://blog.chyla.org/rss',
			'http://blog.creyn.pl/feed/',
			'http://blog.degustudios.com/index.php/feed/',
			'http://blog.e-polecanie.pl/?feed=rss2',
			'http://blog.exmoor.pl/feed',
			'http://blog.forigi.com/feed/',
			'http://blog.gonek.net/feed/',
			'http://blog.jhossa.net/feed/',
			'http://blog.kars7e.io/feed.xml',
			'http://blog.kokosa.net/syndication.axd',
			'http://blog.kurpio.com/feed/',
			'http://blog.lantkowiak.pl/index.php/feed/',
			'http://blog.leszczynski.it/feed/',
			'http://blog.rakaz.pl/feed/',
			'http://blog.roobina.pl/?rss=516047c1-683c-4521-8ffd-143a0a546c85',
			'http://blog.simpleshop.pl/?feed=rss2',
			'http://blog.stanaszek.pl/feed/',
			'http://blog.waldemarbira.pl/feed/',
			'http://blog.yellowmoleproductions.pl/feed/',
			'http://blog.yotkaz.me/feeds/posts/default',
			'http://bnowakowski.pl/en/feed/',
			'http://brozanski.net/index.php/feed/',
			'http://cad.bane.pl/feed/',
			'http://cezary.mcwronka.com.hostingasp.pl/feed/',
			'http://charyzmatyczny-programista.blogspot.com/feeds/posts/default',
			'http://chmielowski.net/feed/',
			'http://cleancodestruggle.blogspot.com/feeds/posts/default',
			'http://coder-log.blogspot.com/feeds/posts/default',
			'http://codestorm.pl/feed/',
			'http://codinghabit.pl/feed/',
			'http://codingpersona.com/feed/',
			'http://cojakodze.pl/feed/',
			'http://commitandrun.pl/feed.xml',
			'http://crynkowski.pl/feed/',
			'http://csharks.blogspot.com/feeds/posts/default',
			'http://czekanski.info/feed/',
			'http://czesio-w-it.2ap.pl/feed/',
			'http://damiankedzior.com/feed/',
			'http://dev.mensfeld.pl/feed/',
			'http://dev30.pl/feed/',
			'http://devbochenek.pl/feed/',
			'http://devfirststeps.blog.pl/feed/',
			'http://donpiekarz.pl/feed.xml',
			'http://doriansobacki.pl/feed/',
			'http://dragonet-therrax.blog.pl/feed/',
			'http://dsp.katafrakt.me/feed.xml',
			'http://dspprojekt.blogspot.com/feeds/posts/default',
			'http://dyzur.blogspot.com/feeds/posts/default',
			'http://dziury-w-calym.pl/feed/',
			'http://emigd.azurewebsites.net/feed.xml',
			'http://epascales.blogspot.com/feeds/posts/default',
			'http://feeds.feedburner.com/PassionateProgram',
			'http://filipcinik.azurewebsites.net/index.php/feed/',
			'http://findfriendsswift.blogspot.com/feeds/posts/default',
			'http://fogielpiotr.blogspot.com/feeds/posts/default',
			'http://foreverframe.pl/feed/',
			'http://ggajos.com/rss.xml',
			'http://gronek.gq/feed/',
			'http://halibuti.blogspot.com/feeds/posts/default',
			'http://hryniewski.net/syndication.axd',
			'http://immora.azurewebsites.net/feed/',
			'http://improsoft.blogspot.com/feeds/posts/default',
			'http://incodable.blogspot.com/feeds/posts/default',
			'http://ionicdsp.eu/?feed=rss2',
			'http://itcraftsman.pl/feed/',
			'http://itka4yk.blogspot.com/feeds/posts/default',
			'http://it-michal-sitko.blogspot.com/feeds/posts/default',
			'http://jagielski.net/feed/',
			'http://jakubfalenczyk.com/feed/',
			'http://jakubskoczen.pl/feed/',
			'http://jaroslawstadnicki.pl/feed/',
			'http://jsdn.pl/feed/',
			'http://justmypassions.pl/?feed=rss2',
			'http://kaolo.azurewebsites.net/index.php/feed/',
			'http://kazaarblog.blogspot.com/feeds/posts/default',
			'http://kduszynski.pl/feed/',
			'http://kkustra.blogspot.com/feeds/posts/default',
			'http://knowakowski.azurewebsites.net/feed/',
			'http://kodikable.pl/rss/',
			'http://koscielniak.me/post/index.xml',
			'http://koscielski.ninja/feed/',
			'http://kotprogramistyczny.pl/feed/',
			'http://kreskadev.azurewebsites.net/rss/',
			'http://krystianbrozek.pl/feed/',
			'http://krzyskowk.postach.io/feed.xml',
			'http://krzysztofabramowicz.com/feed/',
			'http://krzysztofzawistowski.azurewebsites.net/?feed=rss2',
			'http://kubasz.esy.es/feed/',
			'http://langusblog.pl/index.php/feed/',
			'http://lazarusdev.pl/feed/',
			'http://lazybitch.com/feed',
			'http://lion.net.pl/blog/feed.xml',
			'http://liveshare.azurewebsites.net/feed/',
			'http://localwire.pl/feed/',
			'http://lukasz-jankowski.pl/feed/',
			'http://lukaszmocarski.com/feed/',
			'http://lukasz-zborek.pl/feed/',
			'http://maciejskuratowski.com/feed/',
			'http://macieklesiczka.github.io/rss.xml',
			'http://maciektalaska.github.io/atom.xml',
			'http://manisero.net/feed/',
			'http://marcindrobik.pl/Home/Rss',
			'http://marcinklimek.pl/blog/feed/',
			'http://marcinkowalczyk.pl/blog/feed/',
			'http://marcinkruszynski.blogspot.com/feeds/posts/default',
			'http://marcinszyszka.pl/feed/',
			'http://mariuszbartosik.com/feed/',
			'http://martanocon.com/?feed=rss2',
			'http://masakradev.tk/?feed=rss2',
			'http://mateorobiapke.blogspot.com/feeds/posts/default',
			'http://mateuszstanek.pl/feed/',
			'http://matma.github.io/feed.xml',
			'http://mbork.pl/?action=rss',
			'http://mborowy.com/feed/',
			'http://mcupial.pl/feed/',
			'http://memforis.info/feed/',
			'http://metodprojekt.blogspot.com/feeds/posts/default',
			'http://michal.muskala.eu/feed.xml',
			'http://michalgellert.blogspot.com/feeds/posts/default',
			'http://michalogluszka.pl/feed/',
			'http://mieczyk.vilya.pl/feed/',
			'http://milena.mcwronka.com.hostingasp.pl/feed/',
			'http://mmalczewski.pl/index.php/feed/',
			'http://moje-zagwostki.blogspot.com/feeds/posts/default',
			'http://mplonski.prv.pl/feed/',
			'http://mrojecki.azurewebsites.net/rss/',
			'http://msaldak.pl/feed/',
			'http://msnowak.pl/feed/',
			'http://mzieba.com/feed/',
			'http://namiekko.pl/feed/',
			'http://napierala.org.pl/blog/feed/',
			'http://negativeprogrammer.blogspot.com/feeds/posts/default',
			'http://newstech.pl/feed/',
			'http://nicholaszyl.net/feed/',
			'http://nowas.pl/feed/',
			'http://oxbow.pl/feed/',
			'http://parkowanko.blogspot.com/feeds/posts/default',
			'http://paweldobrzanski.pl/feed',
			'http://paweljurczynski.pl/feed/',
			'http://pawelrzepinski.azurewebsites.net/feed/',
			'http://paweltymura.pl/feed/',
			'http://perceptrons.prv.pl/feed/',
			'http://pewudev.pl/feed/',
			'http://pgrzesiek.pl/feed/',
			'http://piatkosia.k4be.pl/wordpress/?feed=rss2',
			'http://piotrgankiewicz.com/feed/',
			'http://piotr-wandycz.pl/feed/',
			'http://plotzwi.com/feed/',
			'http://podziemiazamkul.blogspot.com/feeds/posts/default',
			'http://polak.azurewebsites.net/rss/',
			'http://ppkozlowski.pl/blog/feed/',
			'http://programistka.net/feed/',
			'http://programuje.net/feed/',
			'http://przemek.ciacka.com/feed.xml',
			'http://pumiko.pl/feed.xml',
			'http://pushitapp.azurewebsites.net/index.php/feed/',
			'http://resumees.net/devblog/feed/',
			'http://rutkowski.in/feed/',
			'http://rzeczybezinternetu.blogspot.com/feeds/posts/default',
			'http://sebcza.pl/feed/',
			'http://sirdigital.pl/index.php/feed/',
			'http://slavgamebrew.com/feed/',
			'http://spine.angrybits.pl/?feed=rss2',
			'http://sprobujzmiany.blogspot.com/feeds/posts/default',
			'http://student.agh.edu.pl/~kefas/?feed=rss2',
			'http://sweetprogramming.com/feed/',
			'http://swistak35.com/feed.xml',
			'http://szumiato.pl/feed/',
			'http://szymekk.me/blog/feed/',
			'http://takiarek.com/feed/',
			'http://t-code.pl/atom.xml',
			'http://terianil.blogspot.com/feeds/posts/default',
			'http://tokenbattle.blogspot.com/feeds/posts/default',
			'http://tomasz.dudziak.eu/feed/',
			'http://tomaszjarzynski.pl/feed/',
			'http://tomaszkacmajor.pl/index.php/feed/',
			'http://tomaszkorecki.com/feed/',
			'http://tomaszsokol.pl/feed/',
			'http://toomanyitprojects.azurewebsites.net/feed/',
			'http://tsovek.blogspot.com/feeds/posts/default',
			'http://twitop.azurewebsites.net/index.php/feed/',
			'http://wezewkodzie.blogspot.com/feeds/posts/default',
			'http://whitebear.com.pl/feed/',
			'http://www.andrzejdubaj.com/feed/',
			'http://www.arturnet.pl/feed/',
			'http://www.blog.plotnicki.net/?feed=rss2',
			'http://www.bodolsog.pl/devblog/feed/',
			'http://www.codiferent.pl/feed/',
			'http://www.dedlajn.pl/feeds/posts/default',
			'http://www.devanarch.com/feed/',
			'http://www.diwebsity.com/feed/',
			'http://www.dobreprogramy.pl/djfoxer,Rss',
			'http://www.feliszewski.eu/feed/',
			'http://www.ibpabisiak.pl/?feed=rss2',
			'http://www.karolpysklo.pl/?feed=rss2',
			'http://www.malachowicz.org/?feed=rss2',
			'http://www.marcinwojdak.pl/?feed=rss2',
			'http://www.md-techblog.net.pl/feed/',
			'http://www.mguzdek.pl/feed/',
			'http://www.mikolajdemkow.pl/feed/',
			'http://www.namekdev.net/feed/',
			'http://www.owsiak.org/?feed=rss2',
			'http://www.przemyslawowsianik.net/feed/',
			'http://www.pyrzyk.net/feed/',
			'http://www.sebastiangruchacz.pl/feed/',
			'http://www.select-iot.pl/feed/',
			'http://www.sgierlowski.pl/posts/rssfeed',
			'http://www.straightouttacode.net/rss/',
			'http://www.wearesicc.com/feed/',
			'http://www.webatelier.io/blog.xml',
			'http://www.winiar.pl/blog/feed/',
			'http://www.wlangiewicz.com/feed/',
			'http://www.xaocml.me/feed.xml',
			'http://zszywacz.azurewebsites.net/feed/',
			'http://zelazowy.github.io/feed.xml',
			'https://admincenterblog.wordpress.com/feed/',
			'https://adrianancymon.wordpress.com/feed/',
			'https://alpac4blog.wordpress.com/feed/',
			'https://barloblog.wordpress.com/feed/',
			'https://beabest.wordpress.com/feed/',
			'https://bizon7nt.github.io/feed.xml',
			'https://blog.scooletz.com/feed/',
			'https://branegblog.wordpress.com/feed/',
			'https://brinf.wordpress.com/feed/',
			'https://bzaremba.wordpress.com/feed/',
			'https://chrisseroka.wordpress.com/feed/',
			'https://citygame2016.wordpress.com/feed/',
			'https://damianwojcikblog.wordpress.com/feed/',
			'https://devblog.dymel.pl/feed/',
			'https://devprzemm.wordpress.com/feed/',
			'https://dotnetcoder.wordpress.com/feed/',
			'https://duszekmestre.wordpress.com/feed/',
			'https://dziewczynazpytonem.wordpress.com/feed/',
			'https://fadwick.wordpress.com/feed/',
			'https://gettoknowthebob.wordpress.com/feed/',
			'https://github.com/piotrkowalczuk/charon/commits/master.atom',
			'https://ismenax.wordpress.com/feed/',
			'https://jendaapkatygodniowo.wordpress.com/feed/',
			'https://jporwol.wordpress.com/feed/',
			'https://kamilhawdziejuk.wordpress.com/feed/',
			'https://koniecznuda.wordpress.com/feed/',
			'https://krzysztofmorcinek.wordpress.com/feed/',
			'https://netgwg.wordpress.com/feed/',
			'https://odzeradokoderablog.wordpress.com/feed/',
			'https://onehundredoneblog.wordpress.com/feed/',
			'https://ourtownapp.wordpress.com/feed/',
			'https://pablitoblogblog.wordpress.com/feed/',
			'https://slaviannblog.wordpress.com/feed/',
			'https://stitzdev.wordpress.com/feed/',
			'https://tomaszprasolek.wordpress.com/feed/',
			'https://tomoitblog.wordpress.com/feed/',
			'https://uwagababaprogramuje.wordpress.com/feed/',
			'https://werpuc.wordpress.com/feed/',
			'https://www.blogger.com/feeds/7069982637364525493/posts/default',
			'https://zerojedynka.wordpress.com/feed/',
			'http://novakov.github.io/feed.xml',
			'http://adam.skobo.pl/?feed=rss2',
			'http://www.code-addict.pl/feed',
			'https://gotowalski.wordpress.com/feed/'
        ];
		this.attendeesCount = this.attendeesFeeds.length;
		//this.attendeesCount = 10;
  }
  componentWillMount () {
	  this.attendeesFeeds
		  //.slice(50, 60)
		  .map(function(feedUrl, index){
			  this._getBlogDetails(feedUrl, index);
		  }.bind(this));
  }
  
  _compareByPostsCountDesc (flip, flap) {
		if (flip.entries.length < flap.entries.length) {
					return 1;
				} else if (flip.entries.length > flap.entries.length){
					return -1;
				}
				return 0;
	}
	
	_compareByPostsCountAsc (flip, flap) {
		if (flip.entries.length < flap.entries.length) {
					return -1;
				} else if (flip.entries.length > flap.entries.length){
					return 1;
				}
				return 0;
	}
	
	_compareByLastPostDateDesc (flip, flap) {
		var flipDateError = false;
		var flapDateError = false;
		try {
			var flipDate = LocalDateTime.parse(flip.lastPostDate);
		} catch (parseError) {
			flipDateError = true;
		}
		
		try {
			var flapDate = LocalDateTime.parse(flap.lastPostDate);
		} catch (parseError) {
			flapDateError = true;
		}
		
		if (flipDateError && flapDateError) {
			return 0;
		} else if (flipDateError && !flapDateError) {
			return 1;
		} else if (!flipDateError && flapDateError) {
			return -1;
		} else {
			return -flipDate.compareTo(flapDate);	
		}
	}
	_compareByLastPostDateAsc (flip, flap) {
		var flipDateError = false;
		var flapDateError = false;
		try {
			var flipDate = LocalDateTime.parse(flip.lastPostDate);
		} catch (parseError) {
			flipDateError = true;
		}
		
		try {
			var flapDate = LocalDateTime.parse(flap.lastPostDate);
		} catch (parseError) {
			flapDateError = true;
		}
		
		if (flipDateError && flapDateError) {
			return 0;
		} else if (flipDateError && !flapDateError) {
			return -1;
		} else if (!flipDateError && flapDateError) {
			return 1;
		} else {
			return flipDate.compareTo(flapDate);	
		}
	}
  
	_getLastPostAddress (entries) {
		if (entries.length > 0) {
			  return entries[0].link;
		}
	}  
	
	_getPublishedDate (entries) {
	  if (entries.length > 0) {
		  if (entries[0].publishedDate != '') {
			return format.asString('yyyy-MM-ddThh:mm:ss.000', new Date(entries[0].publishedDate));
		  }
		  return 'Unknown';
	  } else {
		  return 'Never';
	  }
	}
	
	_containsDajsiepoznac (entry) {
		return entry.categories.filter(function (category){
			return category.toLowerCase().search('ozna') > -1;
			 
		}).length > 0;
	}
	
	_lastSevenDays (data) {
		var lastPostDateText = '';
		
		if (data.entries.length > 0) {
		  if (data.entries[0].publishedDate != '') {
			lastPostDateText = format.asString('yyyy-MM-ddThh:mm:ss.000', new Date(data.entries[0].publishedDate));
		  } else {
			  return false;
		  }
	  } else {
		  return false;
	  }
		
		if (lastPostDateText === 'Never') {
			
			return false;
		} else if (lastPostDateText === 'Unknown') {
			return false;
		} else {
			var lastPostDate = LocalDateTime.parse(lastPostDateText).toLocalDate();
			var now = LocalDate.now();
			var periodFromLastPost = Period.between(lastPostDate, now);
			var daysFromLastPost = periodFromLastPost._days;
			var monthsFromLastPost = periodFromLastPost._months;
			return (daysFromLastPost < 7 && monthsFromLastPost === 0);
		}
	}
	
	_lastFourteenDays (data) {
		var lastPostDateText = '';
		
		if (data.entries.length > 0) {
		  if (data.entries[0].publishedDate != '') {
			lastPostDateText = format.asString('yyyy-MM-ddThh:mm:ss.000', new Date(data.entries[0].publishedDate));
		  } else {
			  return false;
		  }
	  } else {
		  return false;
	  }
		if (lastPostDateText === 'Never') {
			return false;
		} else if (lastPostDateText === 'Unknown') {
			return false;
		} else {
			var lastPostDate = LocalDateTime.parse(lastPostDateText).toLocalDate();
			var now = LocalDate.now();
			var periodFromLastPost = Period.between(lastPostDate, now);
			var daysFromLastPost = periodFromLastPost._days;
			var monthsFromLastPost = periodFromLastPost._months;
			return (daysFromLastPost >= 7 && daysFromLastPost < 14 && monthsFromLastPost === 0)
		}
	}
	
	_publishedLongTimeAgo (data) {
		var lastPostDateText = '';
		
		if (data.entries.length > 0) {
		  if (data.entries[0].publishedDate != '') {
			lastPostDateText = format.asString('yyyy-MM-ddThh:mm:ss.000', new Date(data.entries[0].publishedDate));
		  } else {
			  return false;
		  }
	  } else {
		  return false;
	  }
		if (lastPostDateText === 'Never') {

			return false;
		} else if (lastPostDateText === 'Unknown') {
			return true;
		} else {
			var lastPostDate = LocalDateTime.parse(lastPostDateText).toLocalDate();
			var now = LocalDate.now();
			var periodFromLastPost = Period.between(lastPostDate, now);
			var daysFromLastPost = periodFromLastPost._days;
			var monthsFromLastPost = periodFromLastPost._months;
			if (daysFromLastPost < 7 && monthsFromLastPost === 0) {

				return false;
			} else if (daysFromLastPost < 14 && monthsFromLastPost === 0) {

				return false;
			} else {

				return true;
			}
		}
	}
	
	_neverPublished (data) {
		var lastPostDateText = '';
		
		if (data.entries.length > 0) {
		  if (data.entries[0].publishedDate != '') {
			lastPostDateText = format.asString('yyyy-MM-ddThh:mm:ss.000', new Date(data.entries[0].publishedDate));
		  } else {
			  return false;
		  }
	  } else {
		  return true;
	  }
		return (lastPostDateText === 'Never');
	}
	
	_resolveWhichWeek (lastPostDateText) {
		if (lastPostDateText === 'Never') {

			return 'never';
		} else if (lastPostDateText === 'Unknown') {
			return 'never';
		} else {
			var lastPostDate = LocalDateTime.parse(lastPostDateText).toLocalDate();
			var now = LocalDate.now();
			var periodFromLastPost = Period.between(lastPostDate, now);
			var daysFromLastPost = periodFromLastPost._days;
			var monthsFromLastPost = periodFromLastPost._months;
			if (daysFromLastPost < 7 && monthsFromLastPost === 0) {

				return 'thisWeek';
			} else if (daysFromLastPost < 14 && monthsFromLastPost === 0) {

				return 'lastWeek';
			} else {

				return 'longTimeAgo';
			}
		}
	}
	
	_feedUpdated (data) {
		var blogData = null;
		if (data.responseData === null) {
			blogData = new BlogUpdate('','','','',[],'',0,'');
		}
		else {
			var dspEntries = data.responseData.feed.entries.filter(this._containsDajsiepoznac);
			blogData = new BlogUpdate(
				data.responseData.feed.title,
				this._getLastPostAddress(data.responseData.feed.entries),
				data.responseData.feed.link,
				data.responseData.feed.feedUrl,
				data.responseData.feed.entries,
				this._getPublishedDate(data.responseData.feed.entries),
				dspEntries.length,
				this._resolveWhichWeek(this._getPublishedDate(data.responseData.feed.entries)));
		}
		
		this.setState({ 
			blogDetails: this.state.blogDetails.concat([blogData])
		});
	}
  
  _getBlogDetails(feedUrl, index) {
	  $.ajax(
	  {
		  url: 'https://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=-1&q=' + encodeURIComponent(feedUrl),
		  success: this._feedUpdated.bind(this),
		  dataType: 'jsonp'
	  });
  }
  
  shouldComponentUpdate() {
	  return this.state.blogDetails.length % 20 === 0
		|| this.state.blogDetails.length === this.attendeesCount - 1;
  }
  
  _renderProgressBar() {
	  if (this.state.blogDetails.length < this.attendeesCount - 1) {
		  var progressBarLabel = this.state.blogDetails.length + ' of ' + this.attendeesCount;
		  return (<ProgressBar active bsStyle='success' label={ progressBarLabel } now={ (this.state.blogDetails.length / this.attendeesCount) * 100  } />);
	  } else {
		  return (<span />)
	  }
  }
  
  render() {
	  var listItems = this.state.blogDetails.sort(this._compareByLastPostDateDesc).map(
			function(details, index) {
				return (<BlogItem key={index} keyForButton={index} details={ details }/>);
			}.bind(this)
		); 
		
		var progressBarComponent = this._renderProgressBar();
	return (
		<div>
			<p>
			<Label bsStyle='success'>{ this.state.blogDetails.filter(this._lastSevenDays).length } posts published &lt; 7 days ago.</Label>
			<Label bsStyle='warning'>{ this.state.blogDetails.filter(this._lastFourteenDays).length } posts published &gt;= 7 and &lt; 14 days ago.</Label>
			<Label bsStyle='danger'>{ this.state.blogDetails.filter(this._publishedLongTimeAgo).length } posts published &gt;= 14 days ago.</Label>
			<Label bsStyle='info'>{ this.state.blogDetails.filter(this._neverPublished).length } Not active blogs.</Label>
			</p>
			{ progressBarComponent }
			<ListGroup>
				{ listItems }
			</ListGroup>
		</div>
    );
  }
};

export default BlogItemList;