import React from 'react';
import ReactDOM from 'react-dom';
import Dashboard from './components/Dashboard.jsx'
import './css/style.css';

class Index extends React.Component {
  render() {
    return (
      <div>
        <Dashboard/>
      </div>
    );
  }
}

ReactDOM.render(<Index />, document.getElementsByClassName('application')[0]);